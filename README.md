# Parse errors into hopefully more helpful and consistent error codes

## Usage:
Pass your go error into GetErrorCode to receive a new error whose message is one of the codes listed below as according to its type and content.
## Error codes:
### Any error not matched by the conditions below:
ERROR_UNKNOWN
### pgx.PgError (PostgreSQL errors by .Code):
22P02: BERROR_INVALID_INPUT_SYNTAX
23500: DBERROR_INTEGRITY_CONSTRAINT_VIOLATION  
23501: DBERROR_RESTRICT_VIOLATION  
23502: DBERROR_NOT_NULL_VIOLATION  
23503: DBERROR_FOREIGN_KEY_VIOLATION  
23505: DBERROR_UNIQUE_KEY_VIOLATION  
42P01: DBERROR_RELATION_DOES_NOT_EXIST
No match: DBERROR_UNKNOWN (signalling that this error should be tracked down to produce a meaningful error)