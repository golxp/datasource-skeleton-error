package errorcodes

import (
	"errors"
	"log"

	"github.com/jackc/pgx"
)

type RecordNotFoundError struct {
	Message string
}

func (err RecordNotFoundError) Error() string {
	return err.Message
}

func GetErrorCode(err error) error {
	code := "ERROR_UNKNOWN"
	switch err.(type) {
	case pgx.PgError:
		pge, _ := err.(pgx.PgError)
		code = "DBERROR_UNKNOWN"
		switch pge.Code {
		case "22P02":
			code = "DBERROR_INVALID_INPUT_SYNTAX"
		case "23500":
			code = "DBERROR_INTEGRITY_CONSTRAINT_VIOLATION"
		case "23501":
			code = "DBERROR_RESTRICT_VIOLATION"
		case "23502":
			code = "DBERROR_NOT_NULL_VIOLATION"
		case "23503":
			code = "DBERROR_FOREIGN_KEY_VIOLATION"
		case "23505":
			code = "DBERROR_UNIQUE_KEY_VIOLATION"
		case "42601":
			code = "DBERROR_SYNTAX_ERROR"
		case "42P01":
			code = "DBERROR_RELATION_DOES_NOT_EXIST"
		default:
			log.Printf("Code not recognised: %v", pge.Code)
		}
	case RecordNotFoundError:
		code = "DBERROR_RECORD_NOT_FOUND"
	}
	return errors.New(code)
}
